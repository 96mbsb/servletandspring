**LoginDemo** project is implemented using _Servlet and JSP_.
**XmlBasedConfig** is a project that demonstrates _xml based configuration of Spring MVC_.
**EmployeeDetails Maintenance** is a project that demonstrates _Java based configuration of Spring MVC_.
**Student Management** is a project that demonstrates _SpringBoot API_.