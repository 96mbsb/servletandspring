package com.sureshmo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/ValidateUser")
public class ValidateUser extends HttpServlet{
	boolean isValidUser;
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		System.out.println("Validating.."+ id +" user.." ); 
		HttpSession session = request.getSession();
		if(session.getAttribute("isValidUser")==null)
		{
			try {
				isValidUser = validate(id,password);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			isValidUser = (Boolean) session.getAttribute("isValidUser");
		}
		
		session.setAttribute("isValidUser", isValidUser);
		session.setMaxInactiveInterval(100);
		response.sendRedirect("Welcome.jsp");
	}

	private boolean validate(String id, String password) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection mySqlConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/suresh","root","12345");
		Statement queryStatement = mySqlConnection.createStatement();
		ResultSet allUserDetails = queryStatement.executeQuery("SELECT id,password from Credentials");
		
		while(allUserDetails.next()) {
			if(allUserDetails.getString(1).equals(id) )
			{
				return allUserDetails.getString(2).equals(password)?true:false;
			}
		}
		return false;
	}
	
}
