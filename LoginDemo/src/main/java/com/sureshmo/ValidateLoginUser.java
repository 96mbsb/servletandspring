package com.sureshmo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter("/Welcome.jsp")
public class ValidateLoginUser implements Filter {
	boolean isValidUser = false;
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		System.out.println("Validating.."+ id +" user.." ); 
		HttpSession session = ((HttpServletRequest)request).getSession();
		
		if(session.getAttribute("isValidUser")==null || !(Boolean)session.getAttribute("isValidUser") )
		{
			try {
				isValidUser = validate(id,password);
				session.setAttribute("isValidUser", isValidUser);
				session.setMaxInactiveInterval(100);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			isValidUser = (Boolean) session.getAttribute("isValidUser");
		}
		chain.doFilter(request, response);
	}
	
	private boolean validate(String id, String password) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection mySqlConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/suresh","root","12345");
		Statement queryStatement = mySqlConnection.createStatement();
		ResultSet allUserDetails = queryStatement.executeQuery("SELECT id,password from Credentials");
		
		while(allUserDetails.next()) {
			if(allUserDetails.getString(1).equals(id) )
			{
				return allUserDetails.getString(2).equals(password)?true:false;
			}
		}
		return false;
	}
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}


}
