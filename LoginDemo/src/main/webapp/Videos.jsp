<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Videos</title>
</head>
<body>
	<%
// 	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
// 	response.setHeader("Pragma","no-cache");
// 	response.setHeader ("Expires", "0");
		boolean isValidUser;
		try{
			isValidUser =	(Boolean)session.getAttribute("isValidUser");
		}
		catch(Exception e)
		{
			System.out.println(e);
			isValidUser =false;
		}
		if(!isValidUser)
		{
			response.sendRedirect("index.jsp");
		}
	%>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/9uDgJ9_H0gg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</br>
	<form action="Logout" method="post">
		<input type="submit" value="Logout">
	</form>
</body>
</html>