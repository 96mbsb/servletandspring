package com.sureshmo.service;

import java.sql.SQLException;

public interface Movie {
	String watch() throws ClassNotFoundException, SQLException;
}
