package com.sureshmo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;


public class Members {
	public ResultSet getMembers() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection mySqlConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/suresh","root","12345");
		Statement queryStatement = mySqlConnection.createStatement();
		ResultSet allUserDetails = queryStatement.executeQuery("SELECT name from first");
		return allUserDetails;
	}
}
