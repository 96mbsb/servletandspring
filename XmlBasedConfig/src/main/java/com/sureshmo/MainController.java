package com.sureshmo;

import java.sql.SQLException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sureshmo.service.Movie;

@Controller
public class MainController {
	@RequestMapping("/add")
	public ModelAndView add(@RequestParam("val1") int i,@RequestParam("val2") int j) throws ClassNotFoundException, SQLException {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		Movie movie = (Movie) context.getBean("movie");
		String result = movie.watch();
		ModelAndView mv = new ModelAndView();
		mv.addObject("result","	Total number of new viewers:"+ (i+j)+"\n" +result);
		
		mv.setViewName("result");
		return mv;
	}
}
