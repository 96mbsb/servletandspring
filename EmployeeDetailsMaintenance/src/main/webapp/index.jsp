<%@page import="com.sureshmo.dto.Employee"%>
<html>
<body>
	<form action="addEmployee" method="post">
		<br/>ID:<input name="id" type="number" >
		<br/>Name:<input name="name" type="text" >
		<br/>Designation:<input name="position" type="text">
		<br/>Salary:<input name="salary" type="number" >
		<br/><input type="submit" value="Add Employee">
	</form>
	
	<br/>
	<form action="showEmployees">
		<input type="submit" value="Show all employees">
	</form>
	
	<h3>
		<%
			String message = (String)request.getAttribute("result");
			if(message!=null)
			{
				out.println("<br/>Result:<br/>"+message);
			}
			else
			{
				out.println("Welcome");
			}
		%>
	</h3>
</body>
</html>
