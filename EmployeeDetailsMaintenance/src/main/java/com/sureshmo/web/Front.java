package com.sureshmo.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;



@Configuration
@ComponentScan({"com.sureshmo.controller","com.sureshmo.service", "com.sureshmo.dao", "com.sureshmo.dto"})
public class Front{
	
	@Bean
	public InternalResourceViewResolver configureViewResolver() {
		InternalResourceViewResolver irv = new InternalResourceViewResolver();
		irv.setSuffix(".jsp");
		return irv;
	}

}
