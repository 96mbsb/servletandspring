package com.sureshmo.controller;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.sureshmo.dto.Employee;
import com.sureshmo.service.EmployeeService;

@Controller
public class EmployeeController {
	@Autowired
	EmployeeService es;
	
	@Autowired
	Employee e;
	
	//change to request body
	@PostMapping("/addEmployee")
	public ModelAndView addEmployee(@RequestParam("id") int id, @RequestParam("name") String name, @RequestParam("position") String position, @RequestParam("salary") int salary) throws ClassNotFoundException, SQLException {
		ModelAndView mav = new ModelAndView();
		e.setId(id);
		e.setName(name);
		e.setPosition(position);
		e.setSalary(salary);
		String result = es.addEmployee(e);
		mav.addObject("result",result);
		mav.setViewName("index");
		return mav;
	}
	
	@RequestMapping("/showEmployees")
	public ModelAndView showEmployees() throws ClassNotFoundException, SQLException {
		ModelAndView mav = new ModelAndView();
		String employeeDetails = es.showEmployees();
		mav.addObject("resultset",employeeDetails);
		mav.setViewName("result");
		return mav;
	}
}
