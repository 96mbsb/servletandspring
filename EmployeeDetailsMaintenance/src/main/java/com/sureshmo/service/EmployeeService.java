package com.sureshmo.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sureshmo.dao.EmployeeDao;
import com.sureshmo.dto.Employee;

@Service
public class EmployeeService {
	@Autowired
	EmployeeDao dao;
	
	public String addEmployee(Employee e){
		return dao.addEmployee(e);
	}

	public String showEmployees() throws ClassNotFoundException, SQLException {
		
		return dao.showEmployees();
	}

}
