package com.sureshmo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Repository;

import com.sureshmo.dto.Employee;

@Repository
public class EmployeeDao {
	public static Connection mySqlConnection;
	public static Statement queryStatement;
	
	public String addEmployee(Employee employee) 
	{
		try {
			createConnection();
			queryStatement.executeUpdate("INSERT INTO EmployeeDetails VALUES("+employee.getId()+",\""+employee.getName()+"\",\""+employee.getPosition()+"\","+employee.getSalary()+")");
			closeConnection();
			return employee+"<br/> Added successfully..";
		}
		catch(Exception exception)
		{
			return "<br/>Exception"+exception+" occured.. Please try again";
		}
		
	}

	public String showEmployees() throws ClassNotFoundException, SQLException {
		try {			
			createConnection();
			ResultSet employeeDetails = queryStatement.executeQuery("select * from EmployeeDetails");
			String result="";
			while (employeeDetails.next())
				result+="<br/>ID:"+employeeDetails.getInt(1)+" Name:"+employeeDetails.getString(2);
			
			closeConnection();
			return result;
		}
		catch(Exception exception)
		{
			return "<br/>Exception"+exception+" occured.. Please try again";
		}
		
	}
	
	public static void createConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		mySqlConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employee","root","12345");
		queryStatement = mySqlConnection.createStatement();
	}
	
	public static void closeConnection() throws ClassNotFoundException, SQLException {		
		mySqlConnection.close();
	}
}
