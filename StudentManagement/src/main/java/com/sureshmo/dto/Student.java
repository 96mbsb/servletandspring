package com.sureshmo.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
public class Student {
	@Id
	private int rollno;
	
	private String name;
	
	private int mobileno;

	public int getRollno() {
		return rollno;
	}

	public void setRollno(int rollno) {
		this.rollno = rollno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMobileno() {
		return mobileno;
	}

	public void setMobileno(int mobileno) {
		this.mobileno = mobileno;
	}
}
