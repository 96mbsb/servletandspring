package com.sureshmo.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sureshmo.dto.Student;

@Repository
public interface StudentDAO extends CrudRepository<Student, Integer>{
	public Student findByName(String name);

	@Query("SELECT s FROM Student s where id=?1 AND name=?2")
	public Student findByIdAndName(int id, String name);
}
