package com.sureshmo.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sureshmo.dto.Student;
import com.sureshmo.service.StudentService;

@RestController
public class StudentController {
	
	@Autowired
	StudentService service;
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student) {
		return service.addStudent(student);
	}
	
	@GetMapping("getStudent")
	public List<Student> getStudent() {
		return service.getStudent();
	}
	
	@DeleteMapping("deleteStudent")
	public String deleteStudent(@RequestParam("rollno") int rollno)
	{
		return service.deleteStudent(rollno);
	}
	
	@GetMapping("getStudentByName")
	public Student getStudentByName(@RequestParam("name") String name) {
		return service.getStudentByName(name);
	}
	
	@GetMapping("getStudentByIdAndName")
	public Student getStudentByIdAndName(@RequestParam("rollno") int rollno, @RequestParam("name") String name) {
		return service.getStudentByIdAndName(rollno, name);
	}
}
