package com.sureshmo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sureshmo.dao.StudentDAO;
import com.sureshmo.dto.Student;

@Service
public class StudentService {
	@Autowired
	StudentDAO dao;
	public Student addStudent(Student student) {
		return dao.save(student);
	}
	public List<Student> getStudent() {
		return (List<Student>) dao.findAll();
	}
	public String deleteStudent(int rollno) {
		try {
			dao.deleteById(rollno);
			return "Deleted";
		}
		catch(Exception e)
		{
			return "Deletion failed\n"+e+"occured";
		}
	}
	public Student getStudentByName(String name) {
		return dao.findByName(name);
	}
	public Student getStudentByIdAndName(int rollno, String name) {
		return dao.findByIdAndName(rollno, name);
	}
}
